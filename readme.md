# annt's [st](http://st.suckless.org/)'s custom build

```markdown
# st - simple terminal

st is a simple terminal emulator for X which sucks less
```

> **original README @ suckless' [git repo](https://git.suckless.org/st/file/README.html)**

## table of contents

- [patches](#patches)
- [installation](#installation)
- [notes](#notes)

## patches

- [alpha](http://st.suckless.org/patches/alpha/)
- [boxdraw](http://st.suckless.org/patches/boxdraw/)
- [externalpipe](http://st.suckless.org/patches/externalpipe/)
- [font2](http://st.suckless.org/patches/ligatures/)
- [ligatures](https://dwm.suckless.org/patches/centeredwindowname/)
  - ligatures
  - alpha
  - boxdraw
  - scrollback
- [scrollback](http://st.suckless.org/patches/scrollback/)
  - scrollback
  - scrollback-mouse
- [vertcenter](http://st.suckless.org/patches/vertcenter/)
- [xclearwin](http://st.suckless.org/patches/xclearwin/)

## installation

1. `git clone` this repo: (alternatively download manually)

   ```sh
   git clone https://gitlab.com/anntnzrb/st.git
   ```

2. `cd` into corresponding directory & install with `make`:

   ```sh
   make clean install
   ```

## notes

**st** is very sensitive to emojis/glyphs and special characters in general

installing the following packages _may_ fix described issues:

- libxft-bgra
- ttf-symbola
